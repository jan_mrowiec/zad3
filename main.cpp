//https://bitbucket.org/jan_mrowiec/zad3/src/master/
#include <iostream>
#include <bitset>
#include <string>
#include <regex>
using namespace std;


bool reg(string &s, int a[]) //walidacja maski
{
    regex wzorzec("^(\\d{1,3})[.](\\d{1,3})[.](\\d{1,3})[.](\\d{1,3})$"); //wyrazenie regularne sprawdzajace wczytany adres
    regex wzorzec2("([3-9]\\d\\d)|(2[6-9]\\d)|(25[6-9])"); // wyrazenie regularne sprawdzajace czy wartosc nie przekracza wartosci 255
    smatch wynik;
    smatch wyniki;
    if(!regex_search( s, wynik, wzorzec )) //sprawdzenie adresu i zapisanie go do wynik[]
    {
        cout << "Nieprawidlowy adres" << endl;
        return 0;
    }
    for(int i = 1; i <= 4; i++){ //sprawdzenie czy kazada wartosc jest mniejsza od 256
        string k = wynik[i];
        if(regex_search(k, wyniki, wzorzec2))
        {
            cout << "Liczba jest za duza " << wynik[i] << endl;
            return 0;
        }
        a[i - 1] = stoi(k); //zapisanie jako int
    }
    return 1;
}
bool check_mask(int a[])
{
    if(a[0] == 0) //maska nie moze miec samych 0
    {
        cout << "Nieprawidlowa maska" << endl;
        return 0;
    }
    if(a[3] == 255) //maska nie moze miec samych jedynek
    {
        cout << "Nieprawidlowa maska" << endl;
        return 0;
    }
    for(int i = 0; i < 4; i++) //sprawdzenie czy maska ma postac samych jedynek z lewej i samych zer z prawej. Jesli nie wyswietl blad
    {
        if(a[i] != 255)
        {
            unsigned char c = a[i];
            while(c >= 128)
            {
                c = c << 1;
            }
            if(c != 0)
            {
                cout << "Nieprawidlowa maska " << a[i] << endl;
                return 0;
            }
            i++;
            while(i < 4)
            {
                if(a[i] != 0)
                {
                    cout << "Nieprawidlowa maska" << endl;
                    return 0;
                }
                i++;
            }
        }
    }
    return 1;

}

int main()
{
    string adres, maska;
    int a[4], m[4]; //a[] - adres ip, m[] - maska
    cout << "Wpisz adres ip" << endl;
    cin >> adres;
    if(!reg(adres, a)) // walidacja adresu ip
        return -1;
    cout << "Wpisz maska" << endl;
    cin >> maska;
    if(!reg(maska, m)) //walidacja maski jako adres ip
        return -1;
    if(!check_mask(m)) //walidacji maski jako maska
        return -1;
    cout << "Adres ip" << endl;
    cout << "Zapis binarny " << (bitset<8>)(a[0]) << '.' << (bitset<8>)(a[1]) << '.' << (bitset<8>)(a[2]) << '.' << (bitset<8>)(a[3]) << endl;
    cout << "Zapis dziesietny " << a[0] << '.' << a[1] << '.' << a[2] << '.' << a[3] << endl;
    cout << "Zapis heksadecymalny " << hex << a[0] << '.' << a[1] << '.' << a[2] << '.' << a[3] << endl << endl << dec;

    cout << "Adres maski" << endl;
    cout << "Zapis binarny " << (bitset<8>)(m[0]) << '.' << (bitset<8>)(m[1]) << '.' << (bitset<8>)(m[2]) << '.' << (bitset<8>)(m[3]) << endl;
    cout << "Zapis dziesietny " << m[0] << '.' << m[1] << '.' << m[2] << '.' << m[3] << endl;
    cout << "Zapis heksadecymalny " << hex << m[0] << '.' << m[1] << '.' << m[2] << '.' << m[3] << endl << endl << dec;

    for(int i = 0; i < 4; i++) // utworzenie adresu sieci w a[]
        a[i] = a[i] & m[i];
    cout << "Adres sieci" << endl;
    cout << "Zapis binarny " << (bitset<8>)(a[0]) << '.' << (bitset<8>)(a[1]) << '.' << (bitset<8>)(a[2]) << '.' << (bitset<8>)(a[3]) << endl;
    cout << "Zapis dziesietny " << a[0] << '.' << a[1] << '.' << a[2] << '.' << a[3] << endl;
    cout << "Zapis heksadecymalny " << hex << a[0] << '.' << a[1] << '.' << a[2] << '.' << a[3] << endl << endl << dec;
    return 0;
}
